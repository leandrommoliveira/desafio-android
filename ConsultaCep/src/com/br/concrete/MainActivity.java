package com.br.concrete;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.rest.RestService;
import org.springframework.web.client.ResourceAccessException;
import com.br.concrete.entity.Endereco;
import com.br.concrete.mask.Mask;
import com.br.concrete.persist.Internal;
import com.br.concrete.rest.CepClient;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.PopupMenu;
import android.widget.PopupMenu.OnMenuItemClickListener;
import android.widget.TextView;
import android.widget.Toast;

@EActivity(R.layout.activity_main)
public class MainActivity extends Activity implements OnMenuItemClickListener {
	
	private Map<String,Endereco> map;
	private ProgressDialog dialog;
	private Internal internal;
	
	@RestService
	CepClient client;
	
	@ViewById(R.id.textCep)
	EditText textCep;
	
	@ViewById(R.id.textBairro)
	TextView textBairro;
	
	@ViewById(R.id.textCidade)
	TextView textCidade;
	
	@ViewById(R.id.textEstado)
	TextView textEstado;
	
	@ViewById(R.id.textLogradouro)
	TextView textLogradouro;
	
	@ViewById(R.id.textTipoLogradouro)
	TextView textTipoLogradouro;
	
	@AfterViews
	void maskCep(){
		textCep.addTextChangedListener(Mask.insert("##.###-###", textCep));
	}
	
	@Click
	void botaoPesquisar(){
		if(!testConnection()){
			Toast.makeText(getApplicationContext(), R.string.internet_error, Toast.LENGTH_LONG).show();
			return;
		}
		dialog = ProgressDialog.show(this, "",  "Buscando. Por Favor, aguarde...", true);
		buscaCep();
		
	}
	
	@Background
	void buscaCep() {
		try{
			Endereco endereco = client.searchCep(Mask.unmask(textCep.getText().toString()));
			internal = new Internal();
			internal.salvar(this, endereco.toJson());
			atualizaTela(endereco);
			
		}catch(Exception e){
			verificaErro(e);
			return;
		}
	}
	
	@UiThread
	void verificaErro(Exception e){
		dialog.dismiss();
		if(e instanceof ResourceAccessException){
			Toast.makeText(getApplicationContext(), R.string.service_error, Toast.LENGTH_LONG).show();
			return;
		}
		Toast.makeText(getApplicationContext(), R.string.cep_error, Toast.LENGTH_LONG).show();
	}
	
	@Click
	void botaoHistorico(){
		String texto;
		internal = new Internal();
		BufferedReader buffer = internal.ler(this);
		map = new HashMap<String,Endereco>();
		try {
			while((texto = buffer.readLine()) != null){
				Gson gson = new GsonBuilder().create();
	            Endereco p = gson.fromJson(texto, Endereco.class);
	            map.put(p.getCep(), p);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
        PopupMenu popupMenu = new PopupMenu(this, textCep);
        popupMenu.setOnMenuItemClickListener(this);
        for(Entry<String,Endereco> entry : map.entrySet()){
        	String key = entry.getKey();
        	popupMenu.getMenu().add(Menu.NONE, Integer.parseInt(key), Menu.NONE, key);
        }
        popupMenu.show();
	}
	
	@UiThread
	void atualizaTela(Endereco endereco){
		dialog.dismiss();
		textTipoLogradouro.setText("Tipo de Logradouro: " + endereco.getTipoLogradouro());
		textLogradouro.setText("Logradouro: " + endereco.getLogradouro());
		textBairro.setText("Bairro: " + endereco.getBairro());
		textCidade.setText("Cidade: " + endereco.getCidade());
		textEstado.setText("Estado: " + endereco.getEstado());
	}
	
	public boolean testConnection() {
		ConnectivityManager cm = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);

		if (cm.getActiveNetworkInfo() != null){
			return true;
		}
		return false;
	}

	public boolean onMenuItemClick(MenuItem item) {
		dialog = ProgressDialog.show(this, "",  "Buscando. Por Favor, aguarde...", true);
		Endereco endereco = this.map.get(String.valueOf(item.getItemId()));
		atualizaTela(endereco);
		return true;
	}
}
