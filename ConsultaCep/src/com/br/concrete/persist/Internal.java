package com.br.concrete.persist;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

import android.content.Context;

public class Internal {
	String ARQUIVO = "data";

	public void salvar(Context context, String text) {
		try {
			FileOutputStream out = context.openFileOutput(ARQUIVO,
					Context.MODE_APPEND);
			OutputStreamWriter outstream = new OutputStreamWriter(out);
			outstream.write(text);
			outstream.write("\n");
			outstream.flush();
			outstream.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public BufferedReader ler(Context context) {
		BufferedReader bf = null;
		try {
			FileInputStream input = context.openFileInput(ARQUIVO);
			InputStreamReader in = new InputStreamReader(input);
			bf = new BufferedReader(in);
				
		} catch (Exception e) {
			e.printStackTrace();
		}
		return bf;
	}
}
