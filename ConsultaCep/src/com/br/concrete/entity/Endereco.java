package com.br.concrete.entity;

import com.google.gson.annotations.SerializedName;

public class Endereco {
    @SerializedName("tipoDeLogradouro")
    private String tipoLogradouro;

    @SerializedName("logradouro")
    private String logradouro;

    @SerializedName("bairro")
    private String bairro;

    @SerializedName("cidade")
    private String cidade;

    @SerializedName("estado")
    private String estado;
    
    @SerializedName("cep")
    private String cep;
    
    public String getEstado() {
        return estado;
    }

    public String getTipoLogradouro() {
        return tipoLogradouro;
    }

    public String getLogradouro() {
        return logradouro;
    }

    public String getBairro() {
        return bairro;
    }

    public String getCidade() {
        return cidade;
    }
    
    public String getCep() {
        return cep;
    }
    
    
    public String toString(){
    	return this.estado + " - " + this.cidade + " - " + this.bairro + " - " + this.tipoLogradouro + " : " + this.logradouro ; 
    }
    
    public String toJson(){
    	return "{'cep':'"+cep+"','tipoDeLogradouro':'"+tipoLogradouro+"','logradouro':'"+logradouro+"','bairro':'"+bairro+"','cidade':'"+cidade+"','estado':'"+estado+"'}";
    }
}
