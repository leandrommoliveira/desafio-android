package com.br.concrete.rest;

import org.androidannotations.annotations.rest.Get;
import org.androidannotations.annotations.rest.Rest;
import org.springframework.http.converter.json.GsonHttpMessageConverter;

import com.br.concrete.entity.Endereco;

@Rest(rootUrl = "http://correiosapi.apphb.com", converters = {GsonHttpMessageConverter.class})
public interface CepClient {
    @Get("/cep/{cep}")
    Endereco searchCep(String cep);
}
